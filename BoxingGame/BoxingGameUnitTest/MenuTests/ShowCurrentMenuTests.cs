﻿using BoxingGame.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BoxingGameUnitTest.MenuTests
{
    [TestClass]
    public class ShowCurrentMenuTests: MenuTestsBase
    {
        [TestInitialize]
        public void Init()
        {
            BaseInit();
        }

        [TestMethod]
        [Ignore("Ignore a test")]
        public void ShouldPrintContinueMenuIfMenuTypeIsContinue()
        {
            var menuString =
                "create new boxer - enter this command to create a new boxer. You current boxer will be deleted.\n\n" + 
                "search boxer - enter this command to find a boxer for the fight\n\n" +
                "training - enter this command to do trainings that can increase your boxer's characteristics\n\n" +
                "show info - enter this command if to see general information about your boxer\n\n" +
                "exit - enter this command to exit the game. All changes will be saved\n" + 
                "---------------------------------------------------------------------";

            VerifyMenuIsPrinted(MenuType.Continue, menuString);
        }


        [TestMethod]
        [Ignore("Ignore a test")]
        public void ShouldPrintSearchBoxersMenuIfMenuTypeIsSearchBoxers()
        {
            var menuString =
                "choose * - enter this command to choose the boxer you want to fight with. * - boxer's number in the list.\n\n" +
                "back - enter this command to go back to main menu\n" +
                "---------------------------------------------------------------------"; ;

            VerifyMenuIsPrinted(MenuType.SearchBoxers, menuString);
        }

        [TestMethod]
        public void ShouldPrintSearchTrainersMenuIfMenuTypeIsSearchTrainers()
        {
            var menuString =
                "choose * - enter this command to choose the trainer you want to train your boxer. * - boxer's number in the list.\n\n" +
                "back - enter this command to go back to main menu\n" +
                "---------------------------------------------------------------------"; ;

            VerifyMenuIsPrinted(MenuType.SearchTrainers, menuString);
        }
    }
}
