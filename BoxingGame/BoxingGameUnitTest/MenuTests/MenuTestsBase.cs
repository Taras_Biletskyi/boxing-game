﻿using BoxingGame;
using BoxingGame.Enums;
using BoxingGame.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BoxingGameUnitTest.MenuTests
{
    public class MenuTestsBase
    {
        protected Menu menu;
        protected Mock<IGameControl> controlMock;

        public void BaseInit()
        {
            controlMock = new Mock<IGameControl>();
            menu = new Menu(controlMock.Object);
        }

        protected void VerifyMenuIsPrinted(MenuType menuType, string menuString)
        {
            menu.CurrentMenuType = menuType;

            bool success = false;
            if (menuType == MenuType.Start)
                success = menu.ShowStartMenu();
            else
                success = menu.ShowCurrentMenu();

            controlMock.Verify(mock => mock.PrintString(It.Is<string>(arg => arg == menuString)), Times.Once);
        }
    }
}
