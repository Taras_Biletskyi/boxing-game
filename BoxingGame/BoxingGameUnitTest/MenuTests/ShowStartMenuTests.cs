﻿using BoxingGame.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BoxingGameUnitTest.MenuTests
{

    [TestClass]
    public class ShowStartMenuTests:MenuTestsBase
    {
        [TestInitialize]
        public void Init()
        {
            BaseInit();
        }

        [TestMethod]
        public void ShouldPrintStartMenu()
        {
            var menuString = 
                "create boxer - enter this command if you want to create a boxer. " +
                "If you have not created any yet, you will not be able to proceed\n\n" +
                "exit - enter this command to exit the game. All changes will be saved\n" +
                    "---------------------------------------------------------------------";

            VerifyMenuIsPrinted(MenuType.Start, menuString);
        }
    }
}
