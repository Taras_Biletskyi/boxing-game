﻿using BoxingGame;
using BoxingGame.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BoxingGameUnitTest.BoxerFactoryTests
{
    [TestClass]
    public class CreateBoxerTests
    {
        private int defaultAttack = 10;
        private int defaultDefence = 10;
        private int defaultPopularity = 0;
        private int defaultMoney = 1000;
        private Belt[] defaultBelts = null;

        private BoxerFactory boxerFactory;
        private Mock<IGameControl> controlMock;

        [TestInitialize]
        public void Init()
        {
            controlMock = new Mock<IGameControl>();
            boxerFactory = new BoxerFactory(controlMock.Object);
        }

        [TestMethod]
        [Ignore("Ignore a test")]
        public void ShouldReturnBoxerWithSpecifiedParameters()
        {
            var inputFirstName = "Taras";
            var inputSecondName = "Biletskyi";
            var inputNationality = "Ukraine";
            var inputWeight = 70;
            var inputAge = 20;

            MockTheInputSequence(inputFirstName, inputSecondName, inputNationality, inputWeight, inputAge);

            var createdBoxer = boxerFactory.CreateBoxer();

            Assert.AreEqual(inputFirstName, createdBoxer.FirstName);
            Assert.AreEqual(inputSecondName, createdBoxer.SecondName);
            Assert.AreEqual(inputNationality, createdBoxer.Nationality);
            Assert.AreEqual(inputWeight, createdBoxer.Weight);
            Assert.AreEqual(inputAge, createdBoxer.Age);
        }

        [TestMethod]
        public void CreatedBoxerShouldHaveDefaultAttackDefencePopularityMoneyBelts_properties()
        {
            var createdBoxer = boxerFactory.CreateBoxer();

            MockTheInputSequence(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>());
            
            Assert.AreEqual(defaultAttack, createdBoxer.Attack);
            Assert.AreEqual(defaultDefence, createdBoxer.Defence);
            Assert.AreEqual(defaultPopularity, createdBoxer.Popularity);
            Assert.AreEqual(defaultMoney, createdBoxer.Money);
            Assert.AreEqual(defaultBelts, createdBoxer.Belts);
        }

        [TestMethod]
        public void ShoudPrintMessagesDuringTheBoxerCreation()
        {
            string callOrder = "";
            controlMock.Setup(mock => mock.PrintString("Enter first name: ")).Callback(() => callOrder += "1");
            controlMock.Setup(mock => mock.PrintString("Enter second name: ")).Callback(() => callOrder += "2");
            controlMock.Setup(mock => mock.PrintString("Enter nationality: ")).Callback(() => callOrder += "3");
            controlMock.Setup(mock => mock.PrintString("Enter weight: ")).Callback(() => callOrder += "4");
            controlMock.Setup(mock => mock.PrintString("Enter age: ")).Callback(() => callOrder += "5");

            boxerFactory.CreateBoxer();

            Assert.AreEqual("12345", callOrder);

        }
        private void MockTheInputSequence(string firstName, string secondName, string nationality, int weight, int age)
        {
            controlMock.SetupSequence(mock => mock.ReadString())
                .Returns(firstName)
                .Returns(secondName)
                .Returns(nationality);

            controlMock.SetupSequence(mock => mock.ReadInt())
                .Returns(weight)
                .Returns(age);
        }


    }
}
