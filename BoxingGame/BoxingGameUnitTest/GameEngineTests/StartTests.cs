﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BoxingGameTests.GameEngineTests
{
    [TestClass]
    [TestCategory("GameEngine.Start")]
    public class StartTests: GameEngineTestsBase
    {
        [TestInitialize]
        public void Init()
        {
            BaseInit();
        }

        [TestMethod]
        [Ignore("Ignore a test")]
        public void TheMenuShouldBeShownWhenTheGameIsStarted()
        {
            gameEngine.Start();
            controlMock.Setup(mock => mock.ReadInput()).Returns("userInput");

            menuMock.Verify(mock => mock.ShowStartMenu(), Times.Once);
        }

        [TestMethod]
        public void ShouldBeAbleToChooseTheMenuItemAfterMenuWasShown()
        {
            string callOrder = "";
            menuMock.Setup(mock => mock.ShowStartMenu()).Callback(() => callOrder += "1");
            controlMock.Setup(mock => mock.ReadInput()).Callback(()=>callOrder += "2").Returns("userInput");
            gameEngine.Start();

            controlMock.Verify(mock => mock.ReadInput(), Times.Once);
            Assert.AreEqual("12", callOrder);
        }

        [TestMethod]
        [Ignore("Ignore a test")]
        public void ShouldReturnTrueIfTheActionWasSuccessfullyDone()
        {
            var userInput = "create boxer";
            controlMock.Setup(mock => mock.ReadInput()).Returns(userInput);
            bool result = gameEngine.Start();

            Assert.AreEqual(true, result);
        }
    }
}
