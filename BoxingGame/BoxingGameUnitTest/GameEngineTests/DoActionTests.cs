﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BoxingGame;
using System.Collections.Generic;
using Moq;
using BoxingGame.Enums;
using BoxingGame.Interfaces;

namespace BoxingGameTests.GameEngineTests
{
    [TestClass()]
    [TestCategory("GameEngine.DoAction")]
    public class DoActionTests : GameEngineTestsBase
    {

        [TestInitialize]
        public void Init()
        {
            BaseInit();
        }

        [TestMethod()]
        [Ignore("Ignore a test")]
        public void ShouldCallCreateBoxerMethodIfUserInputIsCreate_Boxer()
        {
            string input = "create boxer";
            gameEngine.DoAction(input, MenuType.Start);

            boxerFactoryMock.Verify(mock => mock.CreateBoxer(), Times.Once);
        }

        [TestMethod()]
        [Ignore("Ignore a test")]
        public void ShouldCallCreateBoxerMethodAndChangeCurrentMenuTypeIfBoxerWasCreated()
        {
            string input = "create boxer";

            boxerFactoryMock.Setup(mock => mock.CreateBoxer()).Returns(new Boxer());
            gameEngine.DoAction(input, MenuType.Start);

            Assert.AreNotEqual(null, gameEngine.MyBoxer);
            menuMock.VerifySet(mock => mock.CurrentMenuType = MenuType.Continue);
        }

        [TestMethod()]
        [Ignore("Ignore a test")]
        public void ShouldReturnFalseIfBoxerWasNotCreatedAndKeepCurrentMenuTypeToStart()
        {
            string input = "create boxer";

            gameEngine.DoAction(input, MenuType.Start);

            Assert.AreEqual(null, gameEngine.MyBoxer);
            menuMock.VerifySet(mock => mock.CurrentMenuType = MenuType.Start, Times.Never);
        }

        [TestMethod()]
        public void ShouldCallSearchBoxerMethodIfUserInputIsSearch_Boxers()
        {
            string input = "search boxer";
            gameEngine.DoAction(input, MenuType.Continue);

            boxerServiceMock.Verify(mock => mock.SearchBoxers(), Times.Once);
        }

        [TestMethod()]
        public void ShouldShowSearchedBoxersIfUserInputIsSearch_Boxer()
        {
            string input = "search boxer";
            var boxers = new List<Boxer>(3);
            boxerServiceMock.Setup(mock => mock.SearchBoxers()).Returns(boxers);
            gameEngine.DoAction(input, MenuType.Continue);

            controlMock.Verify(mock => mock.ShowBoxers(It.Is<List<Boxer>>(arg => arg == boxers)), Times.Once);
        }

        [TestMethod]
        public void ShouldShowSearchedTrainersIfUserInputIsTrainings()
        {
            string input = "training";
            var trainers = new List<Trainer>(3);
            trainerServiceMock.Setup(mock => mock.SearchTrainers()).Returns(trainers);
            gameEngine.DoAction(input, MenuType.Continue);

            controlMock.Verify(mock => mock.ShowTrainers(It.Is<List<Trainer>>(arg => arg == trainers)), Times.Once);
        }

        [TestMethod]
        public void ShouldShowInformationAboutCurrentBoxerState()
        {
            string input = "show info";
            gameEngine.DoAction(input, MenuType.Continue);

            controlMock.Verify(mock => mock.ShowCurrentStateInfo(It.IsAny<Boxer>()), Times.Once);
        }

        [TestMethod]
        public void ShouldChangeCurrentMenuForSearchBoxersActionToSearchBoxer()
        {
            string input = "search boxer";
            gameEngine.DoAction(input, MenuType.Continue);

            menuMock.VerifySet(mock => mock.CurrentMenuType = MenuType.SearchBoxers);
        }

        [TestMethod]
        public void ShouldChangeCurrentMenuForSearchedTrainersActionToSearchTrainers()
        {
            string input = "training";
            gameEngine.DoAction(input, MenuType.Continue);

            menuMock.VerifySet(mock => mock.CurrentMenuType = MenuType.SearchTrainers);
        }

        [TestMethod()]
        public void ShouldExitTheGameIfUserInputIsExit()
        {
            string input = "exit";
            gameEngine.DoAction(input, MenuType.Start);
            controlMock.Verify(mock => mock.ExitGame(), Times.Once);

            gameEngine.DoAction(input, MenuType.Continue);
            controlMock.Verify(mock => mock.ExitGame(), Times.Exactly(2));
        }

        [TestMethod()]
        public void ShouldSaveCreatedBoxerInMyBoxerAttribute()
        {
            string input = "create boxer";
            var boxer = new Boxer("F", "S", "U", 70, 20);
            boxerFactoryMock.Setup(mock => mock.CreateBoxer()).Returns(boxer);
            gameEngine.DoAction(input, MenuType.Start);

            Assert.AreEqual(boxer, gameEngine.MyBoxer);
        }

        [TestMethod()]
        public void ShouldChangeCurrentMenuTypeToContinueWhenUserInputedBack()
        {
            string input = "back";
            gameEngine.DoAction(input, MenuType.Start);

            menuMock.VerifySet(mock => mock.CurrentMenuType = MenuType.Continue);
        }

    }
}