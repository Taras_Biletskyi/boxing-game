﻿using BoxingGame.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BoxingGameTests.GameEngineTests
{
    [TestClass]
    public class ContinueTests: GameEngineTestsBase
    {
        [TestInitialize]
        public void Init()
        {
            BaseInit();
        }

        [TestMethod]
        public void ShouldShowContinueMenuWhenTheGameIsContinued()
        {
            gameEngine.Continue();

            menuMock.Verify(mock => mock.ShowCurrentMenu(), Times.Once);
        }

        [TestMethod]
        [Ignore("Ignore a test")]
        public void ShouldToBeAbleToChooseTheMenuItemAfterMenuWasShown()
        {
            string callOrder = "";
            menuMock.Setup(mock => mock.ShowCurrentMenu()).Callback(() => callOrder += "1");
            controlMock.Setup(mock => mock.ReadInput()).Callback(() => callOrder += "2").Returns("userInput");
            gameEngine.Continue();

            controlMock.Verify(mock => mock.ReadInput(), Times.Once);
            Assert.AreEqual("12", callOrder);
        }

        [TestMethod]
        [Ignore("Ignore a test")]
        public void ShouldReturnTrueIfTheActionWasSuccessfullyDone()
        {
            var userInput = "exit";
            controlMock.Setup(mock => mock.ReadInput()).Returns(userInput);
            bool result = gameEngine.Continue();

            Assert.AreEqual(true, result);
        }

        [TestMethod()]
        public void ShouldGiveAccessOnlyToTheSpecifiedMenuOptions()
        {
            string input = "create boxer";
            controlMock.Setup(mock => mock.ReadInput()).Returns(input);
            menuMock.Setup(mock => mock.CurrentMenuType).Returns(MenuType.Continue);
            bool result = gameEngine.Continue();

            Assert.AreEqual(false, result);
        }
    }
}
