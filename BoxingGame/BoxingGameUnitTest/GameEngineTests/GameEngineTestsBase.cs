﻿using BoxingGame;
using BoxingGame.Interfaces;
using Moq;

namespace BoxingGameTests.GameEngineTests
{
    public class GameEngineTestsBase
    {
        protected GameEngine gameEngine;
        protected Mock<IMenu> menuMock;
        protected Mock<IGameControl> controlMock;
        protected Mock<IBoxerFactory> boxerFactoryMock;
        protected Mock<IBoxerService> boxerServiceMock;
        protected Mock<ITrainerService> trainerServiceMock;
        public void BaseInit()
        {
            menuMock = new Mock<IMenu>();
            controlMock = new Mock<IGameControl>();
            boxerFactoryMock = new Mock<IBoxerFactory>();
            boxerServiceMock = new Mock<IBoxerService>();
            trainerServiceMock = new Mock<ITrainerService>();
            gameEngine = new GameEngine(menuMock.Object, controlMock.Object, boxerFactoryMock.Object, boxerServiceMock.Object, trainerServiceMock.Object);
        }
    }
}
