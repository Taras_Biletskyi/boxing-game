﻿using BoxingGame;
using BoxingGame.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingGameUnitTest.GameControlTests
{
    public class GameControlTestsBase
    {
        protected GameControl gameControl;
        protected Mock<IConsole> consoleMock;

        protected void BaseInit()
        {
            consoleMock = new Mock<IConsole>();
            gameControl = new GameControl(consoleMock.Object);
        }
    }
}
