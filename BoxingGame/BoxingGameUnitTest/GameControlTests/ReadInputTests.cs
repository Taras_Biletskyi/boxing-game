﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BoxingGameUnitTest.GameControlTests
{
    [TestClass()]
    public class ReadInputTests:GameControlTestsBase
    {
        [TestInitialize]
        public void Init()
        {
            BaseInit();
        }

        [TestMethod]
        public void ShoudReturnInputThatWasRead()
        {
            var input = "input";
            consoleMock.Setup(mock => mock.ReadLine()).Returns(input);
            var readInput = gameControl.ReadInput();

            consoleMock.Verify(mock => mock.ReadLine(), Times.Once);
            Assert.AreEqual(readInput, input);
        }

        [TestMethod]
        [Ignore("Ignore a test")]
        public void ShoudReturnStringThatWasRead()
        {
            var input = "input";
            consoleMock.Setup(mock => mock.ReadLine()).Returns(input);
            var readInput = gameControl.ReadString();

            consoleMock.Verify(mock => mock.ReadLine(), Times.Once);
            Assert.AreEqual(input, readInput);
        }

        [TestMethod]
        public void ShoudReturnIntThatWasRead()
        {
            var input = "123";
            consoleMock.Setup(mock => mock.ReadLine()).Returns(input);
            var readInput = gameControl.ReadInt();

            consoleMock.Verify(mock => mock.ReadLine(), Times.Once);
            Assert.AreEqual(123, readInput);
        }



    }
}
