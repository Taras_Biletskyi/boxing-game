﻿using System;
using System.Collections.Generic;
using BoxingGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BoxingGameUnitTest.GameControlTests
{
    [TestClass]
    public class ShowOutputTests:GameControlTestsBase
    {
        [TestInitialize]
        public void Init()
        {
            BaseInit();
        }

        [TestMethod]
        public void ShouldShowListOfBoxers()
        {
            List<Boxer> boxers = new List<Boxer>()
            {
                new Boxer("Taras", "Biletskyi", "Ukraine", 100, 56){ Attack = 12, Defence = 15},
                new Boxer("K", "G", "L", 70, 54) { Attack = 45, Defence = 56}
            };

            var listString = "Name Surname Nationality Weight Attack Defence\n\n" +
                             "Taras Biletskyi Ukraine 100 56 12 15\n" +
                             "K G L 70 54 45 56\n";

            gameControl.ShowBoxers(boxers);

            consoleMock.Verify(mock => mock.WriteLine(It.Is<string>(arg => arg == listString)), Times.Once);
        }

        [TestMethod]
        [Ignore("Ignore a test")]
        public void ShouldShowListOfTrainers()
        {
            List<Trainer> trainers = new List<Trainer>()
            {
                new Trainer("Taras", "Biletskyi", 10, 4, 100),
                new Trainer("K", "G", 3, 20, 300)
            };

            var listString = "Name Surname TrainAttack TrainDefence SalaryPerTraining\n\n" +
                             "Taras Biletskyi 10 4 100\n" +
                             "K G 3 20 300\n";

            gameControl.ShowTrainers(trainers);

            consoleMock.Verify(mock => mock.WriteLine(It.Is<string>(arg => arg == listString)), Times.Once);
        }

        [TestMethod]
        public void ShouldShowCurrentStateInfo()
        {
            var boxer = new Boxer("Taras", "Biletskyi", "Ukraine", 100, 56) { Attack = 12, Defence = 15, Money = 100000, Popularity = 34, Belts = null};

            var infoString = "Name: Taras\n" +
                             "Surname: Biletskyi\n" +
                             "Nationality: Ukraine\n" +
                             "Weight: 100\n" +
                             "Attack: 12\n" +
                             "Defence: 15\n" +
                             "Money: 100000\n" +
                             "Popularity: 34\n" +
                             "Belts: no\n";
                             
            gameControl.ShowCurrentStateInfo(boxer);

            consoleMock.Verify(mock => mock.WriteLine(It.Is<string>(arg => arg == infoString)), Times.Once);
        }

        [TestMethod]
        public void ShouldShowYesIfBoxerHaveBelts()
        {
            var boxer = new Boxer("Taras", "Biletskyi", "Ukraine", 100, 56) { Attack = 12, Defence = 15, Money = 100000, Popularity = 34, Belts = new List<Belt>() };

            var infoString = "Name: Taras\n" +
                             "Surname: Biletskyi\n" +
                             "Nationality: Ukraine\n" +
                             "Weight: 100\n" +
                             "Attack: 12\n" +
                             "Defence: 15\n" +
                             "Money: 100000\n" +
                             "Popularity: 34\n" +
                             "Belts: yes\n";

            gameControl.ShowCurrentStateInfo(boxer);

            consoleMock.Verify(mock => mock.WriteLine(It.Is<string>(arg => arg == infoString)), Times.Once);
        }

        [TestMethod]
        public void ShouldPrintString()
        {
            var input = "input string";
            gameControl.PrintString(input);

            consoleMock.Verify(mock => mock.WriteLine(It.Is<string>(arg => arg == input)), Times.Once);
        }
    }
}
