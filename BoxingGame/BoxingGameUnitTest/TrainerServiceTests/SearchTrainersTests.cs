﻿using System.Collections.Generic;
using BoxingGame;
using BoxingGame.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BoxingGameUnitTest.TrainerServiceTests
{
    [TestClass]
    public class SearchTrainersTests
    {
        private TrainerService trainerService;
        private Mock<IDbContext> contextMock;

        [TestInitialize]
        public void Init()
        {
            contextMock = new Mock<IDbContext>();
            trainerService = new TrainerService(contextMock.Object);
        }

        [TestMethod]
        public void ShouldReturnAllBoxers()
        {
            var trainerList = new List<Trainer>() { new Trainer() };
            contextMock.Setup(x => x.Trainers).Returns(trainerList);

            var result = trainerService.SearchTrainers();

            Assert.AreEqual(trainerList, result);
        }
    }
}
