﻿using System.Collections.Generic;
using BoxingGame;
using BoxingGame.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BoxingGameUnitTest.BoxerServiceTests
{
    [TestClass]
    public class SearchBoxersTests
    {
        private BoxerService boxerService;
        private Mock<IDbContext> contextMock;

        [TestInitialize]
        public void Init()
        {
            contextMock = new Mock<IDbContext>();
            boxerService = new BoxerService(contextMock.Object);
        }

        [TestMethod]
        public void ShouldReturnAllBoxers()
        {
            var boxerList = new List<Boxer>() { new Boxer("F", "S", "N", 70, 20) };
            contextMock.Setup(x => x.Boxers).Returns(boxerList);
            
            var result = boxerService.SearchBoxers();

            Assert.AreEqual(boxerList, result);
        }
    }
}