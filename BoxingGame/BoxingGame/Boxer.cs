﻿using System.Collections.Generic;

namespace BoxingGame
{
    public class Boxer
    {
        public Boxer(string firstName, string secondName, string nationality, int weight, int age)
        {
            FirstName = firstName;
            SecondName = secondName;
            Nationality = nationality;
            Weight = weight;
            Age = age;

            Attack = 10;
            Defence = 10;
            Popularity = 0;
            Money = 1000;
            Belts = null;
        }

        public Boxer()
        {
            Attack = 10;
            Defence = 10;
            Popularity = 0;
            Money = 1000;
            Belts = null;
        }

        public string FirstName { get; set; }
        public int Weight { get; set; }
        public string SecondName { get; set; }
        public string Nationality { get; set; }
        public int Age { get; set; }
        public int Attack { get; set; }
        public int Defence { get; set; }
        public int Popularity { get; set; }
        public int Money { get; set; }
        public List<Belt> Belts { get; set; }
    }
}