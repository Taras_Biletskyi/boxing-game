﻿namespace BoxingGame
{
    public class Trainer
    {
        public string FirstName;
        public string SecondName;
        public int AddToAttack;
        public int AddToDefence;
        public int SalaryPerTraining;

        public Trainer(string FirstName, string SecondName, int AddToAttack, int AddToDefence, int SalaryPerTraining)
        {
            this.FirstName = FirstName;
            this.SecondName = SecondName;
            this.AddToAttack = AddToAttack;
            this.AddToDefence = AddToDefence;
            this.SalaryPerTraining = SalaryPerTraining;
        }

        public Trainer()
        {

        }
    }
}