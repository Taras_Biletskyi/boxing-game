﻿using BoxingGame.Enums;
using BoxingGame.Interfaces;

namespace BoxingGame
{
    public class GameEngine : IGameEngine
    {
        private IMenu menu;
        private IGameControl control;
        private IBoxerFactory boxerFactory;
        private IBoxerService boxerService;
        private readonly ITrainerService trainingService;

        public Boxer MyBoxer { get; set; }

        public GameEngine(IMenu menu, IGameControl control, IBoxerFactory boxerFactory, IBoxerService boxerService, ITrainerService trainingService)
        {
            this.menu = menu;
            this.control = control;
            this.boxerFactory = boxerFactory;
            this.boxerService = boxerService;
            this.trainingService = trainingService;
        }

        public bool Start()
        {
            menu.ShowStartMenu();
            var result = ReadAndAct(menu.CurrentMenuType);
            return result;
        }

        public bool Continue()
        {
            menu.ShowCurrentMenu();
            var result = ReadAndAct(menu.CurrentMenuType);
            return result;
        }

        public bool DoAction(string userInput, MenuType menuType)
        {
            if (userInput == null)
                return false;

            if (userInput == "create boxer" && menuType == MenuType.Start)
            {
                MyBoxer = boxerFactory.CreateBoxer();
                if (MyBoxer != null)
                    menu.CurrentMenuType = MenuType.Continue;

                return true;
            }
            else if(userInput == "exit" && (menuType == MenuType.Start || menuType == MenuType.Continue))
            {
                control.ExitGame();
                return true;
            }
            else if(userInput == "search boxer" && menuType == MenuType.Continue)
            {
                var boxers = boxerService.SearchBoxers();
                control.ShowBoxers(boxers);
                menu.CurrentMenuType = MenuType.SearchBoxers;

                return true;
            }
            else if (userInput == "training" && menuType == MenuType.Continue)
            {
                var trainers = trainingService.SearchTrainers();
                control.ShowTrainers(trainers);
                menu.CurrentMenuType = MenuType.SearchTrainers;

                return true;
            }
            else if(userInput == "show info" && menuType == MenuType.Continue)
            {
                control.ShowCurrentStateInfo(MyBoxer);
            }
            else if (userInput == "back")
            {
                menu.CurrentMenuType = MenuType.Continue;
            }

            return false;
        }

        private bool ReadAndAct(MenuType menuType)
        {
            var command = control.ReadInput();
            var result = DoAction(command, menuType);
            return result;
        }
    }
}