﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoxingGame.Enums;
using BoxingGame.Interfaces;

namespace BoxingGame
{
    public class Menu : IMenu
    {
        private IGameControl control;

        public Menu(IGameControl control)
        {
            this.control = control;
        }

        public MenuType CurrentMenuType { get; set; }

        public bool ShowCurrentMenu()
        {
            var success = false;

            if(CurrentMenuType == MenuType.Continue)
            {
                var menuString =
                    "create new boxer - enter this command to create a new boxer. You current boxer will be deleted.\n\n" +
                    "search boxer - enter this command to find a boxer for the fight\n\n" +
                    "training - enter this command to do trainings that can increase your boxer's characteristics\n\n" +
                    "show info - enter this command if to see general information about your boxer\n\n" +
                    "exit - enter this command to exit the game. All changes will be saved\n" +
                    "---------------------------------------------------------------------";

                control.PrintString(menuString);

                success = true;
            }
            else if (CurrentMenuType == MenuType.SearchBoxers)
            {
                var menuString =
                    "choose * - enter this command to choose the boxer you want to fight with. * - boxer's number in the list.\n\n" +
                    "back - enter this command to go back to main menu\n" +
                    "---------------------------------------------------------------------";

                control.PrintString(menuString);

                success = true;
            }
            else if (CurrentMenuType == MenuType.SearchTrainers)
            {
                var menuString =
                    "choose * - enter this command to choose the trainer you want to train your boxer. * - boxer's number in the list.\n\n" +
                    "back - enter this command to go back to main menu\n" +
                    "---------------------------------------------------------------------";

                control.PrintString(menuString);

                success = true;
            }

            return success;
        }

        public bool ShowStartMenu()
        {
            var menuString =
                "create boxer - enter this command if you want to create a boxer. " +
                "If you have not created any yet, you will not be able to proceed\n\n" +
                "exit - enter this command to exit the game. All changes will be saved\n" +
                    "---------------------------------------------------------------------";

            control.PrintString(menuString);

            return true;
        }
    }
}
