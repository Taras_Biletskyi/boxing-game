﻿using BoxingGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingGame.Services
{
    public class DbContext : IDbContext
    {
        public List<Boxer> Boxers {
            get
            {
                return new List<Boxer>()
                {
                    new Boxer("Taras", "Biletskyi", "Ukraine", 70, 20)
                    {
                        Attack = 3,
                        Defence = 6,
                        Popularity = 5,
                        Belts = null,
                        Money = 500
                    },
                    new Boxer("Vasyl", "Lomachenko", "Ukraine", 60, 35)
                    {
                        Attack = 1245,
                        Defence = 1545,
                        Popularity = 650,
                        Belts = new List<Belt>(),
                        Money = 50000000
                    },
                    new Boxer("Oleksandr", "Usyk", "Ukraine", 90, 34)
                    {
                        Attack = 4674,
                        Defence = 7643,
                        Popularity = 459,
                        Belts = new List<Belt>(),
                        Money = 30000000
                    }
                };
            }
        } 

        public List<Trainer> Trainers {
            get
            {
                return new List<Trainer>()
                {
                    new Trainer("Emanuel", "Steward", 10, 4, 1000),
                    new Trainer("Vasyl", "Lomachenko", 60, 35, 2000),
                    new Trainer("Freddie", "Roach", 90, 34, 1500)
                };
            }
        }
    }
}
