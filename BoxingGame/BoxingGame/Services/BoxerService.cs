﻿using BoxingGame.Interfaces;
using System.Collections.Generic;

namespace BoxingGame
{
    public class BoxerService : IBoxerService
    {
        private IDbContext context;

        public BoxerService(IDbContext context)
        {
            this.context = context;
        }

        public List<Boxer> SearchBoxers()
        {
            return context.Boxers;
        }
    }
}
