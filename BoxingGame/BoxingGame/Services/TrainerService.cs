﻿using BoxingGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingGame
{
    public class TrainerService : ITrainerService
    {
        private IDbContext context;

        public TrainerService(IDbContext context)
        {
            this.context = context;
        }

        public List<Trainer> SearchTrainers()
        {
            return context.Trainers;
        }
    }
}
