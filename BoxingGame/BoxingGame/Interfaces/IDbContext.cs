﻿using System.Collections.Generic;

namespace BoxingGame.Interfaces
{
    public interface IDbContext
    {
        List<Boxer> Boxers { get; }
        List<Trainer> Trainers { get; }
    }
}
