﻿using System.Collections.Generic;

namespace BoxingGame.Interfaces
{
    public interface IGameControl
    {
        string ReadInput();
        string ReadString();
        int ReadInt();
        void ExitGame();
        void ShowBoxers(List<Boxer> boxer);
        void ShowTrainers(List<Trainer> trainers);
        void ShowCurrentStateInfo(Boxer boxer);
        void PrintString(string inputString);
    }
}