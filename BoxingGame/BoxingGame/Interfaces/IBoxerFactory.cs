﻿namespace BoxingGame.Interfaces
{
    public interface IBoxerFactory
    {
        Boxer CreateBoxer();
    }
}