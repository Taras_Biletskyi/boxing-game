﻿using System.Collections.Generic;

namespace BoxingGame.Interfaces
{
    public interface IBoxerService
    {
        List<Boxer> SearchBoxers();
    }
}