﻿using BoxingGame.Enums;

namespace BoxingGame.Interfaces
{
    public interface IMenu
    {
        MenuType CurrentMenuType { get; set; }
        bool ShowStartMenu();
        bool ShowCurrentMenu();
    }
}