﻿using BoxingGame.Enums;

namespace BoxingGame.Interfaces
{
    public interface IGameEngine
    {
        Boxer MyBoxer { get; set; }
        bool Continue();
        bool DoAction(string userInput, MenuType menuType);
        bool Start();
    }
}