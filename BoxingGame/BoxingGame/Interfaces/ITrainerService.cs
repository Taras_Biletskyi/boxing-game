﻿using System.Collections.Generic;

namespace BoxingGame.Interfaces
{
    public interface ITrainerService
    {
        List<Trainer> SearchTrainers();
    }
}
