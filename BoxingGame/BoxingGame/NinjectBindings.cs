﻿using BoxingGame.Interfaces;
using BoxingGame.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingGame
{
    public class NinjectBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IGameEngine>().To<GameEngine>();
            Bind<IMenu>().To<Menu>();
            Bind<IBoxerFactory>().To<BoxerFactory>();
            Bind<IBoxerService>().To<BoxerService>();
            Bind<IConsole>().To<Console>();
            Bind<IDbContext>().To<DbContext>();
            Bind<IGameControl>().To<GameControl>();
            Bind<ITrainerService>().To<TrainerService>();
        }
    }
}
