﻿namespace BoxingGame.Enums
{
    public enum MenuType
    {
        Start,
        Continue,
        SearchBoxers,
        SearchTrainers
    }
}
