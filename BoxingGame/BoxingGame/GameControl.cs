﻿using System;
using System.Collections.Generic;
using BoxingGame.Interfaces;

namespace BoxingGame
{
    public class GameControl : IGameControl
    {
        private IConsole console;

        public GameControl(IConsole console)
        {
            this.console = console;
        }

        public void ExitGame()
        {
            Environment.Exit(0);
        }

        public void PrintString(string inputString)
        {
            console.WriteLine(inputString);
        }

        public string ReadInput()
        {
            var result = console.ReadLine();
            return result;
        }

        public int ReadInt()
        {
            return Convert.ToInt32(console.ReadLine());
        }

        public string ReadString()
        {
            var result = console.ReadLine();
            return result;
        }

        public void ShowBoxers(List<Boxer> boxers)
        {
            string outputString = "Name Surname Nationality Weight Attack Defence\n\n";

            foreach (var boxer in boxers)
            {
                outputString += boxer.FirstName + " " + boxer.SecondName + " " + boxer.Nationality + " " +
                    boxer.Weight + " " + boxer.Age + " " + boxer.Attack + " " + boxer.Defence + "\n";
            }

            console.WriteLine(outputString);
        }

        public void ShowCurrentStateInfo(Boxer boxer)
        {
            string hasBelts = "no";
            if (boxer.Belts != null)
                hasBelts = "yes";

            var infoString = "Name: " + boxer.FirstName + "\n" +
                 "Surname: " + boxer.SecondName + "\n" +
                 "Nationality: " + boxer.Nationality + "\n" +
                 "Weight: " + boxer.Weight + "\n" +
                 "Attack: " + boxer.Attack + "\n" +
                 "Defence: " + boxer.Defence + "\n" +
                 "Money: " + boxer.Money + "\n" +
                 "Popularity: " + boxer.Popularity + "\n" +
                 "Belts: " + hasBelts + "\n";

            console.WriteLine(infoString);
        }

        public void ShowTrainers(List<Trainer> trainers)
        {
            string outputString = "Name Surname TrainAttack TrainDefence SalaryPerTraining\n\n";

            foreach (var trainer in trainers)
            {
                outputString += trainer.FirstName + " " + trainer.SecondName + " " + trainer.AddToAttack + " " +
                    trainer.AddToDefence + " " + trainer.SalaryPerTraining + "\n";
            }

            console.WriteLine(outputString);
        }
    }
}
