﻿using BoxingGame.Interfaces;
using BoxingGame.Services;
using Ninject;
using System.Reflection;

namespace BoxingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            var gameEngine = kernel.Get<IGameEngine>();

            var success = gameEngine.Start();
            if (success)
            {
                while (true)
                {
                    var success2 = gameEngine.Continue();
                }
            }
        }
    }
}