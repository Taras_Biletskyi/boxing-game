﻿using BoxingGame.Interfaces;

namespace BoxingGame
{
    public class BoxerFactory : IBoxerFactory
    {
        private IGameControl gameControl;

        public BoxerFactory(IGameControl gameControl)
        {
            this.gameControl = gameControl;
        }

        public Boxer CreateBoxer()
        {
            gameControl.PrintString("Enter first name: ");
            var firstName = gameControl.ReadString();

            gameControl.PrintString("Enter second name: ");
            var secondName = gameControl.ReadString();

            gameControl.PrintString("Enter nationality: ");
            var nationality = gameControl.ReadString();

            gameControl.PrintString("Enter weight: ");
            var weight = gameControl.ReadInt();

            gameControl.PrintString("Enter age: ");
            var age = gameControl.ReadInt();

            return new Boxer(firstName, secondName, nationality, weight, age);
        }
    }
}